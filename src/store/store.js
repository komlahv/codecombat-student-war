import Vue from 'vue';
import Vuex from 'vuex';
import { getRandomStudent } from '../shared/functions';

Vue.use(Vuex);

export const store = data => (new Vuex.Store({
  strict: true,
  state: {
    students: data,
    student1: { id: 'student1', ...getRandomStudent(data) },
    student2: { id: 'student2', ...getRandomStudent(data) },
    winner: {}
  },
  getters: {
    students: (state) => state.students,
    student1: (state) => state.student1,
    student2: (state) => state.student2,
    winner: (state) => state.winner,
  },
  mutations: {
    setStudent: (state, payload) => {
      state[payload.id] = { id: payload.id, ...getRandomStudent(state.students) };
    },
    studentsCompete: (state) => {
      let student1Power = state.student2.HP / state.student1.DPS;
      let student2Power = state.student1.HP / state.student2.DPS;

      student1Power > student2Power ? state.winner = state.student1 : state.winner = state.student2;
    },
    restart: (state) => {
      state.winner = {};
      state.student1 = { id: 'student1', ...getRandomStudent(state.students) };
      state.student2 = { id: 'student2', ...getRandomStudent(state.students) };
    },
  },
  actions: {
    randomizeStudent: async (context, payload) => {
      context.commit('setStudent', payload);
    },
    compete: async (context) => {
      context.commit('studentsCompete');
    },
    restart: async (context) => {
      context.commit('restart');
    }
  }
}));
