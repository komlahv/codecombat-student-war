import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';
// import { store } from './store/store';
import socketio from 'socket.io-client';
import VueSocketIO from 'vue-socket.io';

Vue.config.productionTip = false

Vue.use(VueResource);

Vue.use(
  new VueSocketIO({
    debug: true,
    // connection: socketio('http://localhost:4000'),
    connection: socketio('https://codecombat-studentwar.herokuapp.com/'),
  })
);

new Vue({
  // store: store(data),
  render: h => h(App)
}).$mount('#app')
